#!/usr/bin/env python
##############
# %% IMPORTS
##############

import rospy
import time
import threading

from typing import Tuple

from gazebo_msgs.srv import ApplyBodyWrench, BodyRequest
from geometry_msgs.msg import Point, Vector3, Wrench, WrenchStamped, PoseStamped, Pose, Quaternion

import dynamic_reconfigure.client as clt

##############
# %% GLOBALS
##############
Fext = WrenchStamped(wrench=Wrench(force=Vector3(0,0,0), torque=Vector3(0,0,0)))
Fext_1 = WrenchStamped(wrench=Wrench(force=Vector3(0,0,0), torque=Vector3(0,0,0)))
Fext_2 = WrenchStamped(wrench=Wrench(force=Vector3(0,0,0), torque=Vector3(0,0,0)))
unlock_1 = False
unlock_2 = False

##############
# %% FUNCTIONS
##############

def apply_body_wrench_client(body_name:str, wrench:Wrench, ref_pt:(Point or None)=Point(0,0,0), \
      ref_frame:(str or None)=None, start:(int or None)=None, duration:(int or None)=-1) ->  Tuple[bool, str]:
  """
  Apply a wrench to a body link of a model in gazebo for a specified duration.

  Args:
      body_name:  Gazebo body to apply wrench, body names are prefixed by model name, \
                  e.g. panda::base_link (string)
      wrench:     Wrench applied to the origin of the body (Wrench)
      ref_pt:     Wrench is defined at this location in the reference frame (Point) \
                  default value is (x=0, y=0, z=0)
      ref_frame:  Wrench is defined in the reference frame of this entity (string) \
                  default value set and empty ref, which is equivalent to world ref
      start:      Wrench application start time (int in seconds) \
                  default value set null start time, which leads to as soon as possible
      duration:   Duration of wrench application time (int in seconds) \
                  default value make its continuous without end
  Returns
  --------
      success:    return true if set wrench successful (bool)
      status:     comments if available (string)
  """
  if ref_frame is None:
    ref_frame="" 

  apply_body_wrench = rospy.ServiceProxy('/gazebo/apply_body_wrench', ApplyBodyWrench)
  apply_body_wrench.wait_for_service()
  try:
    ret = apply_body_wrench(body_name, ref_frame, ref_pt, wrench, start, duration)
  except rospy.ServiceException as e:
    print("ApplyBodyWrench Error: %s"%e)  

  return ret

def clear_body_wrench_client(body_name:str) -> None:
  """
  Clear a body link of a model in gazebo of its applied wrench.

  Args:
      body_name:  Gazebo body to clear, body names are prefixed by model name, \
                  e.g. panda::base_link (string)
  Returns
  --------
      None
  """
  clear_body_wrench = rospy.ServiceProxy('gazebo/clear_body_wrenches', BodyRequest)
  clear_body_wrench.wait_for_service()
  try:
    ret = clear_body_wrench(body_name)
  except rospy.ServiceException as e:
    print("ApplyBodyWrench Error: %s"%e)
  #str_buff = ""
  #ret.serialize(buff=str_buff)
  #BodyRequestResponse.serialize(buff=str_buff)
  #BodyRequestResponse.mro()
  ## TODO: add success return value ?

def quickWrench(fx:(float or None)=0, fy:(float or None)=0, fz:(float or None)=0, \
              tx:(float or None)=0, ty:(float or None)=0, tz:(float or None)=0) -> Wrench:
  return Wrench(force=Vector3(x=fx,y=fy,z=fz), torque=Vector3(x=tx,y=ty,z=tz))

def rosWait(rosTime:rospy.Time) -> None:
  while(rospy.get_rostime() < rosTime):
    time.sleep(0.1)

def rosWaitReconfigure(client:clt.Client, timeout:float, new_config:dict) -> Tuple[bool, float]:
  t = time.time()
  config = client.get_configuration(timeout=timeout)
  # rospy.loginfo_throttle_identical(1.0, config)
  while(not new_config.items() <= config.items()):
    if (time.time() - t) >= timeout:
      rospy.logwarn("Reconfigure timeout...") # TODO...
      rospy.loginfo(config)
      return False
    time.sleep(0.01)

  return True, time.time() - t
    
def applyForceBothArmMiddlePoint(body_name:Tuple[str, str], duration:rospy.Duration, \
                                 init_time:rospy.Time, wrench:Wrench):
  apply_body_wrench_client(body_name=body_name[0], duration=duration, start=init_time, \
                           wrench=wrench, ref_pt=Point(y=0.1))
  apply_body_wrench_client(body_name=body_name[1], duration=duration, start=init_time, \
                           wrench=wrench, ref_pt=Point(y=-0.1))

def execExpDoubleArmZ(body_names:Tuple[str, str], duration:rospy.Duration, \
                      init_time:rospy.Time, magn:float, clients:(Tuple[clt.Client,clt.Client] or None)=None) -> rospy.Time: 
  if clients is not None:
    clients[0].update_configuration({"Kp_x":1000, "Kp_y":1000, "Kp_z":1000}) 
    clients[1].update_configuration({"Kp_x":1000, "Kp_y":1000, "Kp_z":1000}) 
  # negative force on z axis
  start_time = init_time
  applyForceBothArmMiddlePoint(body_name=body_names, wrench=quickWrench(fz=-magn), \
    duration=duration, init_time=start_time)
  # positive force on z axis
  start_time = start_time + duration + rospy.Duration(secs = 5.0)
  applyForceBothArmMiddlePoint(body_name=body_names, wrench=quickWrench(fz=magn), \
    duration=duration, init_time=start_time)
  # same positive force on z axis, with reduced "stiffness gain"
  rosWait(start_time + duration + rospy.Duration(secs=5.0))
  if clients is not None:
    clients[0].update_configuration({"Kp_x":250, "Kp_y":250, "Kp_z":250}) 
    clients[0].update_configuration({"Kp_x":250, "Kp_y":250, "Kp_z":250}) 
    start_time = start_time + duration + rospy.Duration(secs = 5.0)
    applyForceBothArmMiddlePoint(body_name=body_names, wrench=quickWrench(fz=magn), \
      duration=duration, init_time=start_time)    

  end_time = start_time + duration
  return end_time

def execExpSingleArmZ(body_name:str,duration:rospy.Duration, init_time:rospy.Time, \
                      magn:float, ref_pt:Point, client:(clt.Client or None)=None) -> rospy.Time: 
  if client is not None:
    client.update_configuration({"Kp_x":1000, "Kp_y":1000, "Kp_z":1000}) 
  if(not rosWaitReconfigure(client, timeout=2.0, new_config={"Kp_x":1000, "Kp_y":1000, "Kp_z":1000})):
    rospy.logerr("Configuration was not set properly")
    return rospy.Time.now()
  # negative force on z axis
  start_time = init_time if init_time >= rospy.Time.now() else rospy.Time.now()
  apply_body_wrench_client(body_name=body_name, wrench=quickWrench(fz=-magn), \
    duration=duration, ref_pt=ref_pt, start=start_time)
  rospy.loginfo("First force sent")
  # positive force on z axis
  start_time = start_time + duration + rospy.Duration(secs = 5.0)
  apply_body_wrench_client(body_name=body_name, wrench=quickWrench(fz=magn), \
    duration=duration, ref_pt=ref_pt, start=start_time)
  # same positive force on z axis, with reduced "stiffness gain"
  rosWait(start_time + duration + rospy.Duration(secs=5.0))
  if client is not None:
    client.update_configuration({"Kp_x":1000, "Kp_y":1000, "Kp_z":250}) 
  rosWaitReconfigure(client, timeout=2.0, new_config={"Kp_x":1000, "Kp_y":1000, "Kp_z":250})
  start_time = start_time + duration + rospy.Duration(secs = 5.0)
  rospy.loginfo("New configuration set")
  apply_body_wrench_client(body_name=body_name, wrench=quickWrench(fz=magn), \
    duration=duration, ref_pt=ref_pt, start=start_time)      

  end_time = start_time + duration
  return end_time

def execExpSingleArmY(body_name:str,duration:rospy.Duration, init_time:rospy.Time, \
                      magn:float, ref_pt:Point, client:clt.Client) -> rospy.Time: 
  client.update_configuration({"Kp_x":1000, "Kp_y":1000, "Kp_z":1000}) 
  # negative force on y axis
  start_time = init_time
  apply_body_wrench_client(body_name=body_name, wrench=quickWrench(fy=-magn), \
    duration=duration, ref_pt=ref_pt, start=start_time)
  # positive force on y axis
  start_time = start_time + duration + rospy.Duration(secs = 5.0)
  apply_body_wrench_client(body_name=body_name, wrench=quickWrench(fy=magn), \
    duration=duration, ref_pt=ref_pt, start=start_time)
  # same positive force on y axis, with reduced "stiffness gain"
  rosWait(start_time + duration + rospy.Duration(secs=5.0))
  client.update_configuration({"Kp_x":250, "Kp_y":250, "Kp_z":250}) 
  start_time = start_time + duration + rospy.Duration(secs = 5.0)
  apply_body_wrench_client(body_name=body_name, wrench=quickWrench(fy=magn), \
    duration=duration, ref_pt=ref_pt, start=start_time)      

  end_time = start_time + duration
  return end_time

def execExpSingleArmX(body_name:str,duration:rospy.Duration, init_time:rospy.Time, \
                      magn:float, ref_pt:Point, client:clt.Client) -> rospy.Time:  
  client.update_configuration({"Kp_x":1000, "Kp_y":1000, "Kp_z":1000}) 
  # negative force on x axis
  start_time = init_time
  apply_body_wrench_client(body_name=body_name, wrench=quickWrench(fx=-magn), \
    duration=duration, ref_pt=ref_pt, start=start_time)
  # positive force on x axis
  start_time = start_time + duration + rospy.Duration(secs = 5.0)
  apply_body_wrench_client(body_name=body_name, wrench=quickWrench(fx=magn), \
    duration=duration, ref_pt=ref_pt, start=start_time)
  # same positive force on x axis, with reduced "stiffness gain"
  rosWait(start_time + duration + rospy.Duration(secs=5.0))
  client.update_configuration({"Kp_x":250, "Kp_y":250, "Kp_z":250}) 
  start_time = start_time + duration + rospy.Duration(secs = 5.0)
  apply_body_wrench_client(body_name=body_name, wrench=quickWrench(fx=magn), \
    duration=duration, ref_pt=ref_pt, start=start_time)      

  end_time = start_time + duration
  return end_time

def get_Fext_1(stamped_wrench):
    global Fext_1, unlock_1
    Fext_1 = stamped_wrench
    unlock_1 = True

def get_Fext_2(stamped_wrench):
    global Fext_2, unlock_2
    Fext_2 = stamped_wrench
    unlock_2 = True

def thread_function(publisher:rospy.Publisher):   
    global Fext, Fext_1, unlock_1, Fext_2, unlock_2
    Fext.header.frame_id = "panda_1_K"
    while(1):
      if unlock_1 and unlock_2:
        Fext.header.stamp = rospy.Time.now() 
        Fext.wrench.force.x = (Fext_1.wrench.force.x + Fext_2.wrench.force.x)/2
        Fext.wrench.force.y = (Fext_1.wrench.force.y + Fext_2.wrench.force.y)/2
        Fext.wrench.force.z = (Fext_1.wrench.force.z + Fext_2.wrench.force.z)/2
        Fext.wrench.torque.x = (Fext_1.wrench.torque.x + Fext_2.wrench.torque.x)/2
        Fext.wrench.torque.y = (Fext_1.wrench.torque.y + Fext_2.wrench.torque.y)/2
        Fext.wrench.torque.z = (Fext_1.wrench.torque.z + Fext_2.wrench.torque.z)/2

        unlock_1 = False
        unlock_2 = False

        publisher.publish(Fext)
##############
# %% MAIN
##############

if __name__ == "__main__":
  #beginning = time.time()
  rospy.init_node('body_wrench_sequence')
  arm_id_base = rospy.get_param('~arm_id', 'panda')
  magn = rospy.get_param('~magnitude', 40.0)/2.0
  exp_nb = rospy.get_param('~experiment', 'dual_arm')

  #arm_nb = rospy.get_param('which_arm', '1')
  arm_id1 = arm_id_base + "_" + "1"
  # client1 = clt.Client(arm_id1+"/torque_control", timeout=30)
  panda1_link_name_base = arm_id1 + "::" + arm_id1 + "_link" 
  if "dual" in exp_nb:
    arm_id2 = arm_id_base + "_" + "2"
    # client2 = clt.Client(arm_id2+"/torque_control", timeout=30)
    panda2_link_name_base = arm_id2 + "::" + arm_id2 + "_link"
  mrk_pose_pub = rospy.Publisher("marker_pose", PoseStamped, queue_size=10)

  rospy.Subscriber(arm_id1 + "/franka_state_controller/F_ext", WrenchStamped, get_Fext_1)
  rospy.Subscriber(arm_id2 + "/franka_state_controller/F_ext", WrenchStamped, get_Fext_2)
  fext_publisher = rospy.Publisher("Fext", WrenchStamped, queue_size=10)
  fext_thread = threading.Thread(target=thread_function, args=(fext_publisher,), daemon=True)

  fext_thread.start()

  for x in range(1,8):
    clear_body_wrench_client(panda1_link_name_base + str(x))
  panda1_link_7 = panda1_link_name_base + "7"
  ref_pt_from_panda_1 = Point(0,0.1,0) 

  if "dual" in exp_nb:
    for x in range(1,8):
      clear_body_wrench_client(panda2_link_name_base + str(x))
    panda2_link_7 = panda2_link_name_base + "7"
    ref_pt_from_panda_2 = Point(0,-0.1,0) # this offset place the reference point between both robots
  
  duration = rospy.Duration(secs = 3.0)

  sim_time = rospy.get_rostime()
  start_time = sim_time + rospy.Duration(secs = 1.0)

  if "single" in exp_nb:
    end_time = execExpSingleArmZ(panda1_link_7, duration, start_time, magn, Point(0,0,0))
    # end_time = execExpSingleArmZ(panda1_link_7, duration, start_time, magn, Point(0,0,0), client1)
    # rosWait(end_time)
    # end_time = execExpSingleArmY(panda1_link_7, duration, end_time + rospy.Duration(5.0), magn, ref_pt_from_panda_1, client1)
    # rosWait(end_time)
    # end_time = execExpSingleArmX(panda1_link_7, duration, end_time + rospy.Duration(5.0), magn, ref_pt_from_panda_1, client1)
  elif "dual" in exp_nb:  
    # force reference point is fixed to the middle point between both robots  
    end_time = execExpDoubleArmZ([panda1_link_7, panda2_link_7], duration, start_time, magn)
    # end_time = execExpDoubleArmZ([panda1_link_7, panda2_link_7], duration, start_time, magn, [client1, client2])
  else:
    rospy.logerr("Unexpected behaviour.")

  input("Press Enter to quit...")

  # st = time.time()
  # client1.update_configuration({"Kp_z":250})
  # dura = rosWaitReconfigure(client1, 5, {"Kp_z":250})[1]
  # rospy.loginfo(time.time() - st)
  # rospy.loginfo(dura)

  # apply_body_wrench_client(body_name=panda1_link_7, wrench=quickWrench(fz=-magn), \
  #   duration=duration, ref_pt=ref_pt_from_panda_1, start=start_time)
  # apply_body_wrench_client(body_name=panda2_link_7, wrench=quickWrench(fz=-magn), \
  #   duration=duration, ref_pt=ref_pt_from_panda_2, start=start_time)

  # start_time = start_time + duration + rospy.Duration(secs = 2.0)

  # apply_body_wrench_client(body_name=panda1_link_7, wrench=quickWrench(fz=magn), \
  #   duration=duration, ref_pt=ref_pt_from_panda_1, start=start_time)
  # apply_body_wrench_client(body_name=panda2_link_7, wrench=quickWrench(fz=magn), \
  #   duration=duration, ref_pt=ref_pt_from_panda_2, start=start_time)
  
  # rosWait(start_time + duration + rospy.Duration(secs=1.0))

  # client1.update_configuration({"Kp_z":250})
  # client2.update_configuration({"Kp_z":250})

  # start_time = start_time + duration + rospy.Duration(secs = 2.0)

  # apply_body_wrench_client(body_name=panda1_link_7, wrench=quickWrench(fz=magn), \
  #   duration=duration, ref_pt=ref_pt_from_panda_1, start=start_time)
  # apply_body_wrench_client(body_name=panda2_link_7, wrench=quickWrench(fz=magn), \
  #   duration=duration, ref_pt=ref_pt_from_panda_2, start=start_time)

  ######################
  ## Move robot endpoint and then apply force wrench again

  # start_time = start_time + duration + rospy.Duration(secs=1.0)
  # rosWait(start_time) 

  # new_pose = Pose(position=Point(x=0,y=0.1,z=0.6), \
  #                 orientation=Quaternion(w=1))
  # mrk_pose_pub.publish(PoseStamped(pose=new_pose))

  # rosWait(start_time + rospy.Duration(secs=3.0))

  # start_time = start_time + rospy.Duration(secs=3.0)

  # apply_body_wrench_client(body_name=panda1_link_7, wrench=quickWrench(fz=magn), \
  #   duration=duration, ref_pt=ref_pt_from_panda_1, start=start_time)
  # apply_body_wrench_client(body_name=panda2_link_7, wrench=quickWrench(fz=magn), \
  #   duration=duration, ref_pt=ref_pt_from_panda_2, start=start_time)
