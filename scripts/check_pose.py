#!/usr/bin/env python
##############
# %% IMPORTS
##############

import signal

import rospy
import rospkg
import subprocess as sbp
from geometry_msgs.msg import WrenchStamped
from std_msgs.msg import Bool
import numpy as np

# from exception import MoveItCommanderException

import pinocchio as pin

from panda_motion import PandaMotion

##############
# %% MACROS
##############
# CONFIG_LIST = [[ 1.68036915e+00, -8.42556356e-01, -1.70973881e+00,
#         -2.13930506e+00, -1.07969015e-01,  3.79470602e+00,
#         -2.31911600e+00],
#        [-1.93554760e-01,  3.06173882e-01, -4.40111614e-01,
#         -2.80159518e+00, -4.67341047e-01,  2.37002797e+00,
#          1.36848751e+00],
#        [ 2.46931497e+00, -1.72460951e+00, -2.14159230e+00,
#         -1.80920431e+00, -1.06736308e+00,  3.82230000e+00,
#         -2.26791575e+00],
#        [ 8.13971087e-02,  8.55975330e-01, -2.56761826e-01,
#         -1.83783075e+00, -1.56460913e+00,  1.78301921e+00,
#          1.11443750e+00],
#        [ 2.27361354e-01, -5.95887054e-01, -1.56687834e-03,
#         -2.91068007e+00,  1.75761101e+00,  7.82218358e-01,
#          2.65778567e+00],
#        [ 8.60768282e-01,  3.65907686e-01, -1.94477183e-01,
#         -2.99398815e+00, -3.18218025e-01,  3.36443051e+00,
#         -5.81069719e-01],
#        [ 1.58440055e-01,  1.10209786e-01,  2.01629895e-01,
#         -2.16193622e+00, -2.90641076e+00,  1.61918628e+00,
#          1.28446961e+00],
#        [-2.25768979e-01, -7.24303249e-01, -1.71976200e-02,
#         -2.78464604e+00, -1.30128381e+00,  1.08897846e+00,
#          5.59417249e-01]]


CONFIG_LIST = [[-0.19355476, 0.306173882, -0.440111614, -2.80159518, -0.467341047, 2.37002797, 1.36848751],
              [0.0813971087, 0.85597533, -0.256761826, -1.83783075, -1.56460913, 1.78301921, 1.1144375], 
              [0.227361354, -0.595887054, -0.00156687834, -2.91068007, 1.75761101, 0.782218358, 2.65778567], 
              [0.860768282, 0.365907686, -0.194477183, -2.99398815, -0.318218025, 3.36443051, -0.581069719], 
              [-0.225768979, -0.724303249, -0.01719762, -2.78464604, -1.30128381, 1.08897846, 0.559417249]]


##############
# %% FUNCTIONS
##############

def change_ctrl_mod(pub:rospy.Publisher, val:bool):
    """
    Switch for either the torque qp control mode with joint configuration regularisation task (True), or
    the torque qp control mode without the joint configuration regularisation task (False)
    """
    ctrl_mode_msg = Bool(val)
    pub.publish(ctrl_mode_msg)

##############
# %% MAIN
##############

if __name__ == "__main__":
  
  panda = PandaMotion("/panda_1/robot_description", "panda_1", "panda_1_arm", load_gripper=False)
  
  panda.setJointVelocityScalingFactor(0.5)

  pub_ctrl_mode = rospy.Publisher('/torque_control_mode/is_joint_regularisation', Bool, queue_size=1)

  rospack = rospkg.RosPack()
  path = rospack.get_path('franka_description') + '/panda.urdf'
  print(path)
  model = pin.buildModelFromUrdf(path)
  tip_id = model.getFrameId("panda_link8")

  data = model.createData()

  qmean = (model.upperPositionLimit.T + model.lowerPositionLimit.T)/2
  change_ctrl_mod(pub_ctrl_mode, True)
  panda.goToJointConfiguration(qmean)

  # file_path = rospack.get_path('dual_panda_setup') + '/scripts/' + file_name
  # file = open(file_path, 'rb')
  # config_list = pck.load(file)

  rospy.loginfo('Starting experimental sequence...')
  valid_configs = [] 
  for idx, config in enumerate(CONFIG_LIST):
    try:
      panda.goToJointConfiguration(config)
      change_ctrl_mod(pub_ctrl_mode, False)
      rospy.sleep(2.0)
      change_ctrl_mod(pub_ctrl_mode, True)
      panda.goToJointConfiguration(qmean)
    except: #MoveItCommanderException
       print("Config: {} failed".format(config))
    else:
       valid_configs.append(config)
        
  rospy.loginfo(valid_configs)
