#!/usr/bin/env python
##############
# %% IMPORTS
##############

import signal

import rospy
import rospkg
import subprocess as sbp
from geometry_msgs.msg import WrenchStamped
from std_msgs.msg import Bool
import numpy as np

import pinocchio as pin

from panda_motion import PandaMotion

##############
# %% MACROS
##############

CONFIG_LIST = [[-0.193554760, 0.3061738820, -0.44011161400, -2.80159518, -0.467341047, 2.370027970, 1.3684875100], 
               [0.0813971087, 0.8559753300, -0.25676182600, -1.83783075, -1.564609130, 1.783019210, 1.1144375000], 
               [0.2273613540, -0.595887054, -0.00156687834, -2.91068007, 1.7576110100, 0.782218358, 2.6577856700]]

NB_REPETITIONS = len(CONFIG_LIST)
ROSBAG_RECORD = False

rosbag_record_process = None

##############
# %% FUNCTIONS
##############

def wrench_msg_init(wrenchstamped:(WrenchStamped or None)=None,frame_id:(str or None)=None, stamp:(rospy.Time or None)=None, seq:(int or None)=None, force:(np.ndarray or None)=None, torque:(np.ndarray or None)=None) -> WrenchStamped:
    if wrenchstamped is None:
        wrench = WrenchStamped()
    else:
        wrench = wrenchstamped
    # header
    if frame_id is not None:
        wrench.header.frame_id = frame_id
    if stamp is not None:
        wrench.header.stamp = stamp
    if seq is not None:
        wrench.header.seq = seq
    # wrench
    if force is not None:
        wrench.wrench.force.x = force[0]
        wrench.wrench.force.y = force[1]
        wrench.wrench.force.z = force[2]
    else:
        wrench.wrench.force.x = 0
        wrench.wrench.force.y = 0
        wrench.wrench.force.z = 0
    if torque is not None:
        wrench.wrench.torque.x = torque[0]
        wrench.wrench.torque.y = torque[1]
        wrench.wrench.torque.z = torque[2]
    else:
        wrench.wrench.torque.x = 0
        wrench.wrench.torque.y = 0
        wrench.wrench.torque.z = 0
    #
    return wrench

def start_rosbag(rosbag_name:str, topics:np.ndarray):
    # bag = rosbag.Bag(rosbag_name, 'w')
    # for topic in topics:
        # bag.write(topic, rospy.Time.now(), rospy.AnyMsg, None)
    global rosbag_record_process
    cmd = ['rosbag', 'record', '-o', rosbag_name] + topics
    rosbag_record_process = sbp.Popen(cmd)
    rospy.loginfo('[{}] Rosbag recording started for topics: {}'.format(rospy.Time.now(), ', '.join(topics)))

def stop_rosbag():
    global rosbag_record_process
    rosbag_record_process.send_signal(signal.SIGINT)
    rosbag_record_process.wait()
    rospy.loginfo('[{}] Rosbag recording stopped'.format(rospy.Time.now()))
    rosbag_record_process = None

def change_ctrl_mod(pub:rospy.Publisher, val:bool):
    """
    Switch for either the torque qp control mode with joint configuration regularisation task (True), or
    the torque qp control mode without the joint configuration regularisation task (False)
    """
    ctrl_mode_msg = Bool(val)
    pub.publish(ctrl_mode_msg)

def init_sequence(q0:np.ndarray, q_list:np.ndarray, panda_motion:PandaMotion, record_base_name:str):
    pub_ctrl_mode = rospy.Publisher('/torque_control_mode/is_joint_regularisation', Bool, queue_size=1)

    topics = ["/panda_1/joint_states", 
              "/external_force",
              "/panda_1/desired_cartesian_pose",
              "/panda_1/franka_state_controller/F_ext",
              "netft_data"]    
    
    change_ctrl_mod(pub_ctrl_mode, True)
    # going to neutral pose
    panda.goToJointConfiguration(q0)
    rospy.sleep(2)
    if ROSBAG_RECORD:
        # launch recording
        rosbag_name = record_base_name + '_initialisation.bag'
        bag = start_rosbag(rosbag_name, topics)
    for q in q_list:
        # going to desired configuration
        panda.goToJointConfiguration(q)
        rospy.sleep(5)
    
    # going to neutral pose
    panda.goToJointConfiguration(q0)
    rospy.sleep(2)

    if ROSBAG_RECORD:
        stop_rosbag()

def weight_sequence(q:np.ndarray, panda_motion:PandaMotion):
    pub_ctrl_mode = rospy.Publisher('/torque_control_mode/is_joint_regularisation', Bool, queue_size=1)

    topics = ["/panda_1/joint_states", 
              "/external_force",
              "/panda_1/desired_cartesian_pose",
              "/panda_1/franka_state_controller/F_ext",
              "netft_data"]    
    
    change_ctrl_mod(pub_ctrl_mode, True)
    panda.goToJointConfiguration(q)
    change_ctrl_mod(pub_ctrl_mode, False)

    input("Press Enter to continue...")

    if ROSBAG_RECORD:
        # launch recording
        rosbag_name = "exp_23_06_28_exp_2kg_weight.bag"
        bag = start_rosbag(rosbag_name, topics)
    
    input("Press Enter to continue...")
    if ROSBAG_RECORD:
        stop_rosbag()


##############
# %% MAIN
##############

if __name__ == "__main__":
    
    panda = PandaMotion("/panda_1/robot_description", "panda_1", "panda_1_arm", load_gripper=False)
    ROSBAG_RECORD = rospy.get_param('~rosbag_record')
    
    panda.setJointVelocityScalingFactor(0.1)

    rospack = rospkg.RosPack()
    path = rospack.get_path('franka_description') + '/panda.urdf'
    model = pin.buildModelFromUrdf(path)
    tip_id = model.getFrameId("panda_link8")

    data = model.createData()

    t0 = rospy.Time.now()

    qmean = (model.upperPositionLimit.T + model.lowerPositionLimit.T)/2
    panda.goToJointConfiguration(qmean)

    rospy.loginfo('Starting initialisation sequence...')

    init_sequence(qmean, CONFIG_LIST, panda, "exp_23_06_28")

    weight_sequence(qmean, panda)

    rospy.loginfo('Experimental sequence took {}s '.format((rospy.Time.now() - t0).to_sec()))

