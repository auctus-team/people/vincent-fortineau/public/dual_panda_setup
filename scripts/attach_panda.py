#!/usr/bin/env python3

import rospy
from dual_panda_setup.srv import Attach, AttachRequest


if __name__ == '__main__':
  rospy.init_node('attach_robots_endpoint_links')
  rospy.loginfo("Creating ServiceProxy to /link_attacher_node/attach")
  attach_srv = rospy.ServiceProxy('/link_attacher_node/attach', Attach)
  attach_srv.wait_for_service()
  rospy.loginfo("Created ServiceProxy to /link_attacher_node/attach")

  arm_id = rospy.get_param('~arm_id', 'panda')

  # Link them
  #rospy.loginfo("Attaching cube1 and cube2")
  req = AttachRequest()
  req.model_name_1 = arm_id + "_1"
  req.link_name_1 =  arm_id + "_1_link7"
  req.model_name_2 = arm_id + "_2"
  req.link_name_2 =  arm_id + "_2_link7"

  attach_srv.call(req)
  # From the shell:
  """
  rosservice call /link_attacher_node/attach "model_name_1: 'cube1'
  link_name_1: 'link'
  model_name_2: 'cube2'
  link_name_2: 'link'"
  """
