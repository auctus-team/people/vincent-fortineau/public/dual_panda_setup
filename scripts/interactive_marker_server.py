#!/usr/bin/env python3

#############
# IMPORTS
#############
import rospy
import tf.transformations
import numpy as np

from interactive_markers.interactive_marker_server import \
    InteractiveMarkerServer, InteractiveMarkerFeedback
from visualization_msgs.msg import InteractiveMarker, \
    InteractiveMarkerControl
from geometry_msgs.msg import PoseStamped, Quaternion, Point
from franka_msgs.msg import FrankaState

marker_pose = PoseStamped()
pose_pub = None
# [[min_x, max_x], [min_y, max_y], [min_z, max_z]]
position_limits = [[-0.1, 0.1], [-0.1, 0.1], [0.2, 0.8]]

#############
# FUNCTIONS
#############

def publisher_callback(msg, link_name):
    marker_pose.header.frame_id = link_name
    marker_pose.header.stamp = rospy.Time(0)
    pose_pub.publish(marker_pose)

def process_feedback(feedback):
    if feedback.event_type == InteractiveMarkerFeedback.POSE_UPDATE:
        marker_pose.pose.position.x = max([min([feedback.pose.position.x,
                                          position_limits[0][1]]),
                                          position_limits[0][0]])
        marker_pose.pose.position.y = max([min([feedback.pose.position.y,
                                          position_limits[1][1]]),
                                          position_limits[1][0]])
        marker_pose.pose.position.z = max([min([feedback.pose.position.z,
                                          position_limits[2][1]]),
                                          position_limits[2][0]])
        marker_pose.pose.orientation = feedback.pose.orientation
    server.applyChanges()

def init_marker(init_marker):
    marker_pose.pose.position.x = max([min([init_marker.pose.position.x,
                                        position_limits[0][1]]),
                                        position_limits[0][0]])
    marker_pose.pose.position.y = max([min([init_marker.pose.position.y,
                                        position_limits[1][1]]),
                                        position_limits[1][0]])
    marker_pose.pose.position.z = max([min([init_marker.pose.position.z,
                                        position_limits[2][1]]),
                                        position_limits[2][0]])
    marker_pose.pose.orientation = init_marker.pose.orientation

def marker_pose_callback(data):    
    marker_pose.pose.position.x = max([min([data.pose.position.x,
                                      position_limits[0][1]]),
                                      position_limits[0][0]])
    marker_pose.pose.position.y = max([min([data.pose.position.y,
                                      position_limits[1][1]]),
                                      position_limits[1][0]])
    marker_pose.pose.position.z = max([min([data.pose.position.z,
                                      position_limits[2][1]]),
                                      position_limits[2][0]])
    marker_pose.pose.orientation = data.pose.orientation

    server.setPose(name="equilibrium_pose", pose=data.pose)
    server.applyChanges()

def wait_for_initial_pose(arm_id):

    msg = rospy.wait_for_message("/"+arm_id+"/franka_state_controller/franka_states",
                                 FrankaState)  # type: FrankaState

    initial_quaternion = \
        tf.transformations.quaternion_from_matrix(
            np.transpose(np.reshape(msg.O_T_EE,
                                    (4, 4))))
    initial_quaternion = initial_quaternion / \
        np.linalg.norm(initial_quaternion)
    marker_pose.pose.orientation.x = initial_quaternion[0]
    marker_pose.pose.orientation.y = initial_quaternion[1]
    marker_pose.pose.orientation.z = initial_quaternion[2]
    marker_pose.pose.orientation.w = initial_quaternion[3]
    marker_pose.pose.position.x = msg.O_T_EE[12]
    marker_pose.pose.position.y = msg.O_T_EE[13]
    marker_pose.pose.position.z = msg.O_T_EE[14]

#############
# MAIN
#############

if __name__ == "__main__":
    rospy.init_node("equilibrium_pose_node")
    # listener = tf.TransformListener()
    arm_id = rospy.get_param("~arm_id", default="panda")

    wait_for_initial_pose(arm_id)

    pose_pub = rospy.Publisher(
        "equilibrium_pose", PoseStamped, queue_size=10)

    position_limits = rospy.get_param("~limits", default=position_limits)

    server = InteractiveMarkerServer("equilibrium_pose_marker")
    int_marker = InteractiveMarker()
    int_marker.header.frame_id = rospy.get_param("~parent_frame", default=arm_id+"link_0")
    int_marker.scale = rospy.get_param("~scale", default=1)
    int_marker.name = "equilibrium_pose"
    int_marker.description = ""
    int_marker.pose = marker_pose.pose
    # int_marker.pose.orientation = Quaternion(w=1)
    # int_marker.pose.position = Point(rospy.get_param("~pose_x", default=0), \
    #                                  rospy.get_param("~pose_y", default=0), \
    #                                  rospy.get_param("~pose_z", default=0) )
    # # marker_pose = copy.deepcopy(int_marker)

    init_marker(int_marker)
    rospy.loginfo("Equilibrium node ready.")

    pose_sub = rospy.Subscriber(
        "marker_pose", PoseStamped, marker_pose_callback)

    # run pose publisher
    rospy.Timer(rospy.Duration(0.005),
                lambda msg: publisher_callback(msg, int_marker.header.frame_id))

    # insert a box
    control = InteractiveMarkerControl()
    control.orientation.w = 1
    control.orientation.x = 1
    control.orientation.y = 0
    control.orientation.z = 0
    control.name = "rotate_x"
    control.interaction_mode = InteractiveMarkerControl.ROTATE_AXIS
    int_marker.controls.append(control)

    control = InteractiveMarkerControl()
    control.orientation.w = 1
    control.orientation.x = 1
    control.orientation.y = 0
    control.orientation.z = 0
    control.name = "move_x"
    control.interaction_mode = InteractiveMarkerControl.MOVE_AXIS
    int_marker.controls.append(control)
    control = InteractiveMarkerControl()
    control.orientation.w = 1
    control.orientation.x = 0
    control.orientation.y = 1
    control.orientation.z = 0
    control.name = "rotate_y"
    control.interaction_mode = InteractiveMarkerControl.ROTATE_AXIS
    int_marker.controls.append(control)
    control = InteractiveMarkerControl()
    control.orientation.w = 1
    control.orientation.x = 0
    control.orientation.y = 1
    control.orientation.z = 0
    control.name = "move_y"
    control.interaction_mode = InteractiveMarkerControl.MOVE_AXIS
    int_marker.controls.append(control)
    control = InteractiveMarkerControl()
    control.orientation.w = 1
    control.orientation.x = 0
    control.orientation.y = 0
    control.orientation.z = 1
    control.name = "rotate_z"
    control.interaction_mode = InteractiveMarkerControl.ROTATE_AXIS
    int_marker.controls.append(control)
    control = InteractiveMarkerControl()
    control.orientation.w = 1
    control.orientation.x = 0
    control.orientation.y = 0
    control.orientation.z = 1
    control.name = "move_z"
    control.interaction_mode = InteractiveMarkerControl.MOVE_AXIS
    int_marker.controls.append(control)

    server.insert(int_marker, process_feedback)
    
    server.applyChanges()

    rospy.spin()
