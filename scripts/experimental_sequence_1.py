#!/usr/bin/env python
##############
# %% IMPORTS
##############

import signal

import rospy
import rospkg
import subprocess as sbp
from gazebo_msgs.srv import ApplyBodyWrench
from geometry_msgs.msg import WrenchStamped, Point
from std_msgs.msg import Bool
import numpy as np

import pinocchio as pin

from panda_motion import PandaMotion

##############
# %% MACROS
##############

# CONFIG_LIST = [[-0.193554760, 0.3061738820, -0.44011161400, -2.80159518, -0.467341047, 2.370027970, 1.3684875100], 
#                [0.0813971087, 0.8559753300, -0.25676182600, -1.83783075, -1.564609130, 1.783019210, 1.1144375000], 
#                [0.2273613540, -0.595887054, -0.00156687834, -2.91068007, 1.7576110100, 0.782218358, 2.6577856700], 
#                [0.8607682820, 0.3659076860, -0.19447718300, -2.99398815, -0.318218025, 3.364430510, -0.581069719], 
#                [-0.225768979, -0.724303249, -0.01719762000, -2.78464604, -1.301283810, 1.088978460, 0.5594172490]]


CONFIG_LIST = [[-0.193554760, 0.3061738820, -0.44011161400, -2.80159518, -0.467341047, 2.370027970, 1.3684875100], 
               [0.0813971087, 0.8559753300, -0.25676182600, -1.83783075, -1.564609130, 1.783019210, 1.1144375000], 
               [0.2273613540, -0.595887054, -0.00156687834, -2.91068007, 1.7576110100, 0.782218358, 2.6577856700], 
             #  [0.8607682820, 0.3659076860, -0.19447718300, -2.99398815, -0.318218025, 3.364430510, -0.581069719], 
               [-0.225768979, -0.724303249, -0.01719762000, -2.78464604, -1.301283810, 1.088978460, 0.5594172490]]

MAGN_LIST = [10, -10, 25, -25, 40]
AXES = ['x', 'y', 'z']
PERT_DURATION = 15 # 2 seconds
FREQ = 100 # 100 Hz
NB_REPETITIONS = len(CONFIG_LIST)
ROSBAG_RECORD = False
IS_BODY_WRENCH = False

rosbag_record_process = None

##############
# %% FUNCTIONS
##############

def wrench_msg_init(wrenchstamped:(WrenchStamped or None)=None,frame_id:(str or None)=None, stamp:(rospy.Time or None)=None, seq:(int or None)=None, force:(np.ndarray or None)=None, torque:(np.ndarray or None)=None) -> WrenchStamped:
    if wrenchstamped is None:
        wrench = WrenchStamped()
    else:
        wrench = wrenchstamped
    # header
    if frame_id is not None:
        wrench.header.frame_id = frame_id
    if stamp is not None:
        wrench.header.stamp = stamp
    if seq is not None:
        wrench.header.seq = seq
    # wrench
    if force is not None:
        wrench.wrench.force.x = force[0]
        wrench.wrench.force.y = force[1]
        wrench.wrench.force.z = force[2]
    else:
        wrench.wrench.force.x = 0
        wrench.wrench.force.y = 0
        wrench.wrench.force.z = 0
    if torque is not None:
        wrench.wrench.torque.x = torque[0]
        wrench.wrench.torque.y = torque[1]
        wrench.wrench.torque.z = torque[2]
    else:
        wrench.wrench.torque.x = 0
        wrench.wrench.torque.y = 0
        wrench.wrench.torque.z = 0
    #
    return wrench

def start_rosbag(rosbag_name:str, topics:np.ndarray):
    # bag = rosbag.Bag(rosbag_name, 'w')
    # for topic in topics:
        # bag.write(topic, rospy.Time.now(), rospy.AnyMsg, None)
    global rosbag_record_process
    cmd = ['rosbag', 'record', '-o', rosbag_name] + topics
    rosbag_record_process = sbp.Popen(cmd)
    rospy.loginfo('[{}] Rosbag recording started for topics: {}'.format(rospy.Time.now(), ', '.join(topics)))

def stop_rosbag():
    global rosbag_record_process
    rosbag_record_process.send_signal(signal.SIGINT)
    rosbag_record_process.wait()
    rospy.loginfo('[{}] Rosbag recording stopped'.format(rospy.Time.now()))
    rosbag_record_process = None

def generate_force_step(duration:(float or rospy.Duration), frequency:float, magnitude:float, axis:(int or str), publisher:(rospy.Publisher), service:(rospy.ServiceProxy or None)=None):
    
    step = 1/frequency
    if type(axis) is str:
        if axis.lower() == 'x':
            axis = 0
        elif axis.lower() == 'y':
            axis = 1
        elif axis.lower() == 'z':
            axis = 2
        else:
            raise ValueError
    
    if type(duration) is rospy.Duration:
        duration = duration.to_sec()

    force_mag = np.zeros(3)
    force_mag[axis] = magnitude

    wrench_msg_pert = wrench_msg_init(frame_id='panda_1_link8', force=force_mag, torque=np.zeros(3))
    wrench_msg_pert_inv_conv = wrench_msg_init(frame_id='panda_1_link8', force=-force_mag, torque=np.zeros(3))
    wrench_msg_0 = wrench_msg_init(frame_id='panda_1_link8', force=np.zeros(3), torque=np.zeros(3))

    time = np.arange(0, duration, step)
    sleep_time = rospy.Duration(step)

    wrench_msg_0.header.stamp = rospy.Time.now()
    publisher.publish(wrench_msg_0)

    if service is not None:
      service.wait_for_service()
      ret = service('panda_1_link7', "", Point(x=0.1, z=0.107), wrench_msg_pert_inv_conv.wrench, None, rospy.Duration(duration))

    for t in time:
        rospy.sleep(sleep_time)
        wrench_msg_pert.header.stamp = rospy.Time.now()
        publisher.publish(wrench_msg_pert) 
        
    rospy.sleep(sleep_time)
    wrench_msg_0.header.stamp = rospy.Time.now()
    publisher.publish(wrench_msg_0)

def change_ctrl_mod(pub:rospy.Publisher, val:bool):
    """
    Switch for either the torque qp control mode with joint configuration regularisation task (True), or
    the torque qp control mode without the joint configuration regularisation task (False)
    """
    ctrl_mode_msg = Bool(val)
    pub.publish(ctrl_mode_msg)

def perturbation_sequence(q0:np.ndarray, q:np.ndarray, panda_motion:PandaMotion, magnitudes_list:np.ndarray, record_base_name:str, is_body_wrench:bool=False):

    if(is_body_wrench):
      pub_force = rospy.Publisher('/external_force_tmp', WrenchStamped, queue_size=1)
      serv_force = rospy.ServiceProxy('/gazebo/apply_body_wrench', ApplyBodyWrench)
    else:
      pub_force = rospy.Publisher('/external_force', WrenchStamped, queue_size=1)
      serv_force = None

    pub_ctrl_mode = rospy.Publisher('/torque_control_mode/is_joint_regularisation', Bool, queue_size=1)
    wrench_msg = wrench_msg_init(frame_id="panda_1_joint8", stamp=rospy.Time.now())
    pub_force.publish(wrench_msg)
    change_ctrl_mod(pub_ctrl_mode, True)

    topics = ["/panda_1/franka_state_controller/joint_states", 
              "/external_force",
              "/panda_1/desired_cartesian_pose"]
    
    axes = ['x', 'y', 'z']

    for magn in magnitudes_list:
        for axe in range(2,3):
            # ctrl mode
            change_ctrl_mod(pub_ctrl_mode, True)
            # going to neutral pose
            panda.goToJointConfiguration(q0)
            # going to desired configuration
            panda.goToJointConfiguration(q)
            if ROSBAG_RECORD:
                # launch recording
                rosbag_name = record_base_name + '_' + str(magn) + 'N_' + AXES[axe] + '.bag'
                bag = start_rosbag(rosbag_name, topics)
            change_ctrl_mod(pub_ctrl_mode, False)
            rospy.sleep(4)
            # 
            wrench_msg.header.stamp = rospy.Time.now()
            pub_force.publish(wrench_msg)
            # set perturbation
            generate_force_step(PERT_DURATION, FREQ, magn, axe, pub_force, serv_force)
            # stop recording
            rospy.sleep(4)
            change_ctrl_mod(pub_ctrl_mode, True)
            if ROSBAG_RECORD:
                stop_rosbag()
    # return to neutral pose after the sequence is executed
    panda.goToJointConfiguration(q0)

##############
# %% MAIN
##############

if __name__ == "__main__":
    
    panda = PandaMotion("/panda_1/robot_description", "panda_1", "panda_1_arm", load_gripper=False)
    ROSBAG_RECORD = rospy.get_param('~rosbag_record')
    IS_BODY_WRENCH = rospy.get_param('~use_body_wrench')
    
    panda.setJointVelocityScalingFactor(0.3)

    # rospy.init_node('experimental_sequence')
    # arm_id = rospy.get_param('~arm_id', 'panda_1')

    rospack = rospkg.RosPack()
    path = rospack.get_path('franka_description') + '/panda.urdf'
    print(path)
    model = pin.buildModelFromUrdf(path)
    tip_id = model.getFrameId("panda_link8")

    data = model.createData()

    t0 = rospy.Time.now()

    qmean = (model.upperPositionLimit.T + model.lowerPositionLimit.T)/2
    panda.goToJointConfiguration(qmean)
    panda.goToJointConfiguration(qmean)

    # perturbation_sequence(qmean, qmean, panda, [21], "config_neutral")

    rospy.loginfo('Starting experimental sequence...')
    for idx, config in enumerate(CONFIG_LIST):
        perturbation_sequence(qmean, config, panda, MAGN_LIST, "config_n" + str(idx))
        # if body wrench is activated, the perturbation sequence are sequentially both generated using gazebo apply body wrench service and torque
        if IS_BODY_WRENCH:
            perturbation_sequence(qmean, config, panda, MAGN_LIST, "config_n" + str(idx), IS_BODY_WRENCH)
        

        # perturbation_sequence(qmean, config, panda, MAGN_LIST, "config_n" + str(idx))
        # rospy.loginfo('Sequence n{}/{} finished execution.'.format(idx+1, NB_REPETITIONS))

    rospy.loginfo('Experimental sequence took {}s '.format((rospy.Time.now() - t0).to_sec()))