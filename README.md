# Dual Panda Setup

This package allows to simulate two panda robots in gazebo, having their endpoints linked with a virtual fixed joint.
Both robot are, by default, using a joint torque controller with a [QP formulation](https://gitlab.inria.fr/auctus-team/components/control/torque_qp), that has also been implemented for the panda robot in the [panda_qp_control](https://gitlab.inria.fr/auctus-team/components/robots/panda/panda_qp_control) package. 

The main task is achieved at the endpoint level, with a target position $x^{traj}$ to maintain, and both the target velocity $\dot{x}^{traj}$ and acceleration $\ddot{x}^{traj}$ set to 0. This main task achieved with a PID controller will provide an impedance behaviour to each robots. As shown for a simplified version of this control strategy [here](https://gitlab.inria.fr/auctus-team/people/vincent-fortineau/private/error-polytopes/-/blob/master/error_polytopes_analysis.ipynb), the apparent stiffness of each robot is both related to the proportionnal gain of the PID, and the configuration.

Having both robots rigidly linked will change their endpoint impedance. The coupled stiffness will sum up, if we consider the center of mass of the link between both robots.

<div align="center">
<img src="misc/gazebo_example.png"  style="width:50%" >
</div>

## Installation

For the installation, it is assumed that ROS noetic is already installed. First update tools:

```
sudo apt update
sudo apt install python3-rosdep python3-catkin-tools build-essential cmake git libpoco-dev libeigen3-dev
```

Then create a catkin workspace, with franka emika, qp controller, and this setup packages:

```
mkdir -p panda_ws/src
cd panda_ws
git clone https://gitlab.inria.fr/auctus-team/people/vincent-fortineau/public/panda-ws.git src
catkin config --init --extend /opt/ros/noetic
cd src
git submodule update --init --recursive
cd ..
rosdep install --from-paths src --ignore-src -r -y
catkin build
source devel/setup.bash
```

## Getting started

For simulation:
```
roslaunch dual_panda_setup run.launch  
```
## Project status
In progress... Only implemented for gazebo simulation for now
The panda_qp_control package still needs a little upgrade for this project to properly work (to allow custom arm id).